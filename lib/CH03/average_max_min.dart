import 'dart:io';

import 'dart:math';

void main()
{
  int n,total, maximum, minimum;
  double average;
  List<int> grades = new List();

  stdout. write("How many courses are you taking? ");
  n = int.parse(stdin.readLineSync());

  for(int i=0; i<n; i++)
    {
      stdout.write("Grade of course ${i+1} is: ");
      grades.add(int.parse(stdin.readLineSync()));
    }

  total = grades.reduce((a,b) => a+b);
  average = total / n;
  maximum = grades.reduce(max);
  minimum = grades.reduce(min);


  print("The grades are $grades");
  print("The sum of the grades is: $total");
  print("The average of the grades is: $average");
  print("The maximum of the grades is: $maximum");
  print("The minimum of the grades is: $minimum");

}