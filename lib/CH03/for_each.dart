import 'dart:io';

void main() {
  List<String> list = new List<String>();
  list.add('one');
  list.add('two');
  list.addAll(['twelve', 'two', 'one']);
  print("List is: ");
  list.forEach((element) => stdout.write("$element-"));

  print("\nSet is: ");
  Set<String> set = Set.from(list);
  set.forEach((element) => stdout.write("$element-"));


}
/*Output is
List is:
one-two-twelve-two-one-
Set is:
one-two-twelve-
*/