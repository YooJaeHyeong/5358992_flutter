import 'dart:io';

void main() {
  int quit;
  stdout.write("Enter the number of rows for list: ");
  int row = int.parse(stdin.readLineSync());
  stdout.write("Enter the number of col for list: ");
  int col = int.parse(stdin.readLineSync());

  List<List<String>> numbers = new List.generate(row, (i) => new List(col));

  for (int i = 0; i < row; i++) {
    for (int j = 0; j < col; j++) {
      numbers[i][j] = "$i$j";
      stdout.write(" ${numbers[i][j]}");
    }
    print("");
  }
}
