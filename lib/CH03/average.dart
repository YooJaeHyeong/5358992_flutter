// This program computes the sum and average of grades
import 'dart:io';
void main(){
  // Declaring appropriate variable to store valuse
  int n, total;
  double average;
  List<int> grades = new List();

  // Prompt user as to how many courses they are taking
  stdout.write("How many courses are you taking? ");
  n = int.parse(stdin.readLineSync());

  // For loop to get grades of each course
  for(int i = 0; i < n; i++){
    stdout.write("Grade of course ${i+1} is: ");
    grades.add(int.parse(stdin.readLineSync()));
  }

  total = grades.reduce((a, b) => a + b); // calculating total
  average = total / n; // calculating average
  // Displaying the result
  print("The grades are $grades");
  print("The sum of the grades is: $total");
  print("The average of the grades is: $average");
}