/** This program computes an estimate of pi by simulating dart
 *  onto a square*/
import 'dart:math';

void main(){
  // Declaring variables to store values
  int TRIES = 20000, hits = 0; // TRIES> # of dart throws
  double r, x, y; // r> random num, x> x-coordinate, y>y-coordinate

  var random = Random.secure(); // secure random object

  for(int i = 0; i < TRIES; i++){

    r = random.nextDouble(); // number between 0 and 1
    x = -1 + 2 * r; // x-coordinate is calculated

    r = random.nextDouble();
    y = -1 + 2 * r; // y-coordinate is calculated

    if(x * x + y * y <= 1) // <= 1 inside circle
      hits = hits + 1;
  }
  // Estimating and displaying the PI result
  print("The PI estimate is=> ${4.0 * hits / TRIES}");
}