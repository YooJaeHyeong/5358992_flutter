// This program determines the fee for a cat or dog pet tag.
// It uses nested if/else statements.
import 'dart:io';
void main()
{
  String pet;         // "cat" or "dog"
  String spayed;        // 'y' or 'n'

// Get pet type and spaying information
  stdout.write("Enter the pet type (cat or dog): ");
  pet = stdin.readLineSync();
  stdout.write("Has the pet been spayed or neutered (y/n)? ");
  spayed = stdin.readLineSync();

// Determine the pet tag fee
  if (pet == "cat" ){
    if (spayed == 'y')
      stdout.write("Fee is \$4.00 ");
    else
      stdout.write("Fee is \$8.00 ");
  }
  else if (pet == "dog"){
    if (spayed == 'y')
      stdout.write("Fee is \$6.00 ");
    else
      stdout.write("Fee is \$12.00 ");
  } else
    stdout.write("Only cats and dogs need pet tags. ");
}
