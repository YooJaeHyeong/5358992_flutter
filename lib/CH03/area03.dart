/** Under CH03  create area03 complete the code
 *  Using the give template This program will calculate the area of
 *  Square * Rectangle * Circle * Right Triangle
 *  modify the area02 code using switch statment*/
import 'dart:io';
import 'dart:math';


void main() {

  // DEFINE THE CONSTANT PI HERE AND SET ITS VALUE TO 3.14159
  // DEFINE THE CONSTANT NAME OF AUTHOR OF THE PROGRAM
  // DEFINE THE CONSTANT STUDENT ID OF AUTHOR OF THE PROGRAM
  // DECLARE ALL NEEDED VARIABLES HERE. GIVE EACH ONE A DESCRIPTIVE
  // NAME AND AN APPROPRIATE DATA TYPE.
   double pi = 3.14159, area, length, width, radius, baseStraight, height;
   String name = "Name: YooJaeHyeong", id ="Student ID: 5358992";
   String dm; // display menu

  //DISPLAY NAME AND ID OF AUTHOR OF THE PROGRAM
   print(name);
   print(id);
  // WRITE STATEMENTS HERE TO DISPLAY THE 4 MENU CHOICES.
  print("*** Choose the number   ***"
        "\n*** 1 -- square         ***"
        "\n*** 2 -- rectangle      ***"
        "\n*** 3 -- circle         ***"
        "\n*** 4 -- right triangle ***"
        "\n*** 5 -- quit           ***" );
  // WRITE A STATEMENT HERE TO INPUT THE USER'S MENU CHOICE.
  stdout.write("Choose a valid menu:");
  dm = stdin.readLineSync();

  var number = int.parse(dm);   //trans string to int

  // USE AN SWITCH IF STATEMENT TO OBTAIN ANY NEEDED INPUT INFORMATION
  // AND COMPUTE AND DISPLAY THE AREA FOR EACH VALID MENU CHOICE.
  switch(number)
  {
    case 1 :
      stdout.write("Type the length of the sides of the square: ");
      length = double.parse(stdin.readLineSync());
      area = length * length;
      stdout.write("The square area is $area");
      break;

    case 2 :
      stdout.write("Type the width: ");
      width = double.parse(stdin.readLineSync());
      stdout.write("Type the length: ");
      length = double.parse(stdin.readLineSync());
      area = width * length;
      stdout.write("The rectangle area is $area");
      break;

    case 3:
      stdout.write("Type the radius of circle:");
      radius = double.parse(stdin.readLineSync());
      area = pi * radius;
      stdout.write("The circle area is $area");
      break;

    case 4:
      stdout.write("Type a base straight line of Right Triangle:");
      baseStraight = double.parse(stdin.readLineSync());
      stdout.write("Type a Height of Right Triangle:");
      height = double.parse(stdin.readLineSync());
      area = 0.5*baseStraight*height;
      stdout.write("The Right Triangle area is $area");
      break;

    case 5:
      stdout.write("Exit the Program.");
      break;

  }

// IF AN INVALID MENU CHOICE WAS ENTERED, AN ERROR MESSAGE SHOULD
// BE DISPLAYED BY DEFAULT
  if(number>5){
    stdout.write("This is Invalid Number.\nChoose the number 1 to 5.");
  }
}