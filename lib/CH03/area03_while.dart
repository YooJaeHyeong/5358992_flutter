import 'dart:io';
import 'dart:math';

void main() {
  double pi = 3.14159,
      area,length, width,
      radius, baseStraight,
      height;
  String name = "Name: YooJaeHyeong",
      id = "Student ID: 5358992",
      dm;

  print(name);
  print(id);
  print("*** Choose the number   ***"
      "\n*** 1 -- square         ***"
      "\n*** 2 -- rectangle      ***"
      "\n*** 3 -- circle         ***"
      "\n*** 4 -- right triangle ***"
      "\n*** 5 -- quit           ***");

  stdout.write("Choose a number: ");
  dm = stdin.readLineSync();

  var number = int.parse(dm);

  while (number < 1 || number > 5) {
    stdout.write("Invalid number, enter again: "); // line 25, choose a number
    number = int.parse(stdin.readLineSync());
  }

    switch (number) {
      case 1 :
        stdout.write("Type the length of the sides of the square: ");
        length = double.parse(stdin.readLineSync());
        area = length * length;
        stdout.write("The square area is $area");
        break;

      case 2 :
        stdout.write("Type the width: ");
        width = double.parse(stdin.readLineSync());
        stdout.write("Type the length: ");
        length = double.parse(stdin.readLineSync());
        area = width * length;
        stdout.write("The rectangle area is $area");
        break;

      case 3:
        stdout.write("Type the radius of circle:");
        radius = double.parse(stdin.readLineSync());
        area = pi * radius;
        stdout.write("The circle area is $area");
        break;

      case 4:
        stdout.write("Type a base straight line of Right Triangle:");
        baseStraight = double.parse(stdin.readLineSync());
        stdout.write("Type a Height of Right Triangle:");
        height = double.parse(stdin.readLineSync());
        area = 0.5 * baseStraight * height;
        stdout.write("The Right Triangle area is $area");
        break;

      case 5:
        stdout.write("Exit the Program.");
        break;
    }
  }