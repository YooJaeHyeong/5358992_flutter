import 'dart:io';

void main()
{
  int row = 6, col = 6;
  List<List<String>> strs = new List.generate(row, (i) => new List(col));

  for(int i=0; i<row; i++)
    {
      for(int j=0; j<col; j++)
      {
        strs[i][j] = "$i$j";
        stdout.write(" ${strs[i][j]}");
      }
      print("");
    }
}