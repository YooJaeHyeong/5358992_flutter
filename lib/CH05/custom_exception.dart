import 'exception.dart';

void main() {
  print('start');

  Map<String, int> enlisted = {
    "Tom": 21,
    "Dick": 37,
    "Harry": 51,
    "Mark": 52
  };

  Map<String, int> qualified = new Map();

  enlisted.forEach((name, age){
    try {
      assert(age < 50, throw new TooOldForServiceException(name));
      qualified[name] = age;
    }
    catch (e) {
      print(e);
    }
  });

  print('finish:  \n${enlisted.length} of ${qualified.length}'
      'candidates are valid.');
  print("List of qualified: $qualified");
}