import 'employee_interfaces.dart';

class Employee implements Person, Profession{
  @override
  int age;

  @override
  String name;

  @override
  String profession;

  @override
  double salary;

  @override
  set personAge(int age) => this.age = age;

  @override
  int get personAge{return this.age;}

  @override
  set personName(String name) => this.name = name;

  @override
  String get personName{return this.name;}

  @override
  set personProfession(String prof) => this.profession = prof;

  @override
  String get personProfession{return this.profession;}

  @override
  set personSalary(double salary) => this.salary = salary;

  @override
  double get personSalary{return this.personSalary;}

}