import 'employee.dart';

main(){
  Employee emp = new Employee();

  emp.personName = 'Keith';
  emp.personAge = 30;
  emp.personProfession = "System Analyst";
  emp.personSalary = 25000;

  print("Dart implementing"
        "Multiple Interface Example");

  print(emp.personName);
  print(emp.personAge);
  print(emp.personProfession);
  print(emp.personSalary);
}