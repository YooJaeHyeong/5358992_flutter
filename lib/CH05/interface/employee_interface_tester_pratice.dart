import 'employee.dart';

main(){
  Employee emp = new Employee();

  emp.personName = 'JaeHyoeng';
  emp.personAge = 25;
  emp.personProfession = "Undergraduate Students";
  emp.personSalary = 0.0;

  print("Dart implementing"
      "Multiple Interface Example");

  print(emp.personName);
  print(emp.personAge);
  print(emp.personProfession);
  print(emp.personSalary);
}