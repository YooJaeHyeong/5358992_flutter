import 'dart:async';

String countUp(int count)
{
  print('Start count up');
  StringBuffer sb = new StringBuffer();

  for(int i=0; i<count; i++)
    {
      sb.write(" ${i}");
    }
  print('Finish count up');
  return sb.toString();
}

void countUpAsynchronously(int count) async
{
  print('Async operation start');
  String value = await
      new Future((){return countUp(count);});

  print('Async operation succeeded: ${value}');
}

void main()
{
  print('Start main');
  countUpAsynchronously(100);
  print('Finish main');
}