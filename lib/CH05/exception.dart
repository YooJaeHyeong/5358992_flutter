class TooOldForServiceException implements Exception{
  String _name;
  TooOldForServiceException(this._name);
  @override
  String toString() {
    return "$_name is too old for military service.";
  }
}