import 'dart:io';
import 'choice_question.dart';
import 'package:flutter_5358992/CH04/questions.dart';

void main()
{
  Questions q =new Questions();
  ChoiceQuestions cq = new ChoiceQuestions();

  q.setQuestionText("Who is the inventor of Dart?");
  q.setQuestionAns("Lar is Bak".toUpperCase());

  presentQuestion(q);

  cq.setQuestionAns("In What Year was the Dart introduced?");
  cq.addChoice("2015", false);
  cq.addChoice("2008", false);
  cq.addChoice("2012", true);
  cq.addChoice("2020", false);

  presentQuestion(cq);

}
void presentQuestion (Questions questions)
{
  questions.display();
  stdout.write("Your answer is: ");
  String response = stdin.readLineSync();
  print(questions.checkAnswer(response.toUpperCase()));
}