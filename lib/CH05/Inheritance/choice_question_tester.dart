import 'dart:io';
import 'choice_question.dart';


void main(){
  ChoiceQuestions cq = new ChoiceQuestions();

  cq.setQuestionText("In what year was the Drat introduced?");

  cq.addChoice("2015", false);
  cq.addChoice("2008", false);
  cq.addChoice("2012", true);
  cq.addChoice("2020", false);

  cq.display();
  stdout.write("Your Answer is: ");
  String response = stdin.readLineSync();
  print(cq.checkAnswer(response.toUpperCase()));

}