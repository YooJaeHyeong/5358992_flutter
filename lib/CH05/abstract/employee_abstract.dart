abstract class Employee
{
  void getEmpInfo();
}

class Manager extends Employee
{
  void getEmpInfo()
  {
    print("I Am Manager");
  }
}
class Engineer extends Employee
{
  void getEmpInfo()
  {
    print("I Am Engineer");
  }
}