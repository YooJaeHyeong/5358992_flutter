import 'employee_abstract.dart';

void main()
{
  Manager mng = new Manager();
  Engineer eng = new Engineer();
  print("Abstract Class Example.");
  mng.getEmpInfo();
  eng.getEmpInfo();
}