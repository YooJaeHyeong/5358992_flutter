import 'package:flutter/material.dart';
import 'package:flutter_5358992/mini_project/mp_student.dart';
import 'package:flutter_5358992/mini_project/mp_student_Helper.dart';

class MPStudentDetail extends StatefulWidget {
  final int id;
  const MPStudentDetail({Key key, @required this.id}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _MPStudentDetailState();
}

class _MPStudentDetailState extends State<MPStudentDetail> {
  final GlobalKey<FormState> _formStateKey = GlobalKey<FormState>();
  Future<List<Student2>> students2;
  Future<Student2> _student2;
  List<Student2> student2;
  int studentsIdForUpdate;
  bool isUpdate = false;
  DBHelperMP dbHelperMP;
  String _studentName;
  String _studentDepartment;
  String _studentDoB;
  String _studentLanguage;
  String _studentGender;
  final _studentNameController = TextEditingController();
  final _studentDepartmentController = TextEditingController();
  final _studentDoBController = TextEditingController();
  final _studentLanguageController = TextEditingController();
  final _studentGenderController = TextEditingController();

  @override
  void initState() {
    super.initState();
    dbHelperMP = DBHelperMP();
    refreshStudentList(studentsIdForUpdate);
  }

  refreshStudentList(int id) {
    setState(() {
      _student2 = dbHelperMP.getInfo(id);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Student View"),
      ),
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.all(16.0),
          child: Column(
            children: <Widget>[
              new Expanded(
                child: Text(dbHelperMP.getInfo(studentsIdForUpdate).toString()),
              ),
              delete(student2)
            ],
          ),
        ),
      ),
    );
  }

  Widget delete(List<Student2> student) {
    return Column(children: <Widget>[
      Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          RaisedButton(
              color: Colors.lightGreen,
              child: Text(
                "UPDATE",
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () {
                if (_formStateKey.currentState.validate()) {
                  _formStateKey.currentState.save();
                  dbHelperMP
                      .update(Student2(
                          studentsIdForUpdate,
                          _studentName,
                          _studentDepartment,
                          _studentDoB,
                          _studentLanguage,
                          _studentGender))
                      .then((data) {
                    setState(() {
                      isUpdate = false;
                    });
                  });
                }
                _studentNameController.text = '';
                _studentDepartmentController.text = '';
                _studentDoBController.text = '';
                _studentLanguageController.text = '';
                _studentGenderController.text = '';
                student.map((student) => dbHelperMP.update(student));
                refreshStudentList(studentsIdForUpdate);
              }),
          SizedBox(width: 20.0),
          RaisedButton(
              color: Colors.red,
              child: Text(
                "DELETE",
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () {
                student.map((student) => dbHelperMP.delete(student.id));
                refreshStudentList(studentsIdForUpdate);
                return false;
              }),
        ],
      ),
    ]);
  }
}
