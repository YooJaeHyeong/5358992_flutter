import 'package:flutter/material.dart';
import 'package:flutter_5358992/mini_project/mp_student_home.dart';

void main() => runApp(SLApp());

class SLApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Student List',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.lightGreen,
      ),
      home: MPStudentHome(),
    );
  }
}
