import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'mp_student_Helper.dart';
import 'mp_student.dart';

class MPStudentADD extends StatefulWidget {
  const MPStudentADD({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _MPStudentADDState();
}

class _MPStudentADDState extends State<MPStudentADD> {
  final GlobalKey<FormState> _formStateKey = GlobalKey<FormState>();
  Student2 _student2;
  Future<List<Student2>> students2;
  List<String> _gender = [];
  int studentsIdForUpdate;
  String _studentName;
  String _studentDepartment;
  String _studentDoB;
  String _studentLanguage;
  String _studentGender;
  bool isLanguageKorean = false;
  bool isLanguageEnglish = false;
  bool isLanguageChinese = false;
  bool isUpdate = false;
  DBHelperMP dbHelperMP;
  final _studentNameController = TextEditingController();
  final _studentDepartmentController = TextEditingController();
  final _studentDoBController = TextEditingController();
  final _studentLanguageController = TextEditingController();
  final _studentGenderController = TextEditingController();

  int _radioGroupValue;

  void _changeLanguage() {
    setState(() {
      if (_formStateKey.currentState.validate()) {
        _formStateKey.currentState.save();
      }
    });
  }

  @override
  void initState() {
    super.initState();
    dbHelperMP = DBHelperMP();
    refreshStudentList();
    _radioGroupValue = 0;
  }

  refreshStudentList() {
    setState(() {
      students2 = dbHelperMP.getStudent();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Form(
                key: _formStateKey,
                autovalidateMode: AutovalidateMode.always,
                onChanged: _changeLanguage,
                child: Padding(
                  padding: EdgeInsets.only(left: 10, right: 10, bottom: 10),
                  child: Column(
                    children: <Widget>[
                      TextFormField(
                        validator: (value) =>
                            value.isEmpty ? 'Please Enter Student Name' : null,
                        onSaved: (value) => _studentName = value,
                        controller: _studentNameController,
                        decoration: InputDecoration(
                          labelText: "Student Name",
                          labelStyle: TextStyle(
                            color: Colors.lightGreen,
                          ),
                        ),
                      ),
                      TextFormField(
                        validator: (value) => value.isEmpty
                            ? 'Please Enter Student Department'
                            : null,
                        onSaved: (value) => _studentDepartment = value,
                        controller: _studentDepartmentController,
                        decoration: InputDecoration(
                          labelText: "Student Department",
                          labelStyle: TextStyle(
                            color: Colors.lightGreen,
                          ),
                        ),
                      ),
                      TextFormField(
                        validator: (value) =>
                            value.isEmpty ? 'Please Select Day of Birth' : null,
                        onSaved: (value) => _studentDoB = value,
                        controller: _studentDoBController,
                        decoration: InputDecoration(
                          labelText: "Student DoB",
                          labelStyle: TextStyle(color: Colors.lightGreen),
                        ),
                        onTap: () async {
                          DateTime date = DateTime(1990);
                          FocusScope.of(context).requestFocus(new FocusNode());
                          date = await showDatePicker(
                              context: context,
                              initialDate: DateTime.now(),
                              firstDate: DateTime(1950),
                              lastDate: DateTime(2030));
                          _studentDoBController.text = date.toString();
                        },
                      ),
                      SizedBox(height: 5.0),
                      Text(
                        'Language',
                        style: TextStyle(
                            fontSize: 16.0,
                            decoration: isLanguageKorean == true
                                ? TextDecoration.none
                                : TextDecoration.none),
                      ),
                      CheckboxListTile(
                        title: Text('Korean'),
                        value: isLanguageKorean,
                        selected: isLanguageKorean,
                        onChanged: (val) async {
                          String language1;
                          FocusScope.of(context).requestFocus(new FocusNode());
                          setState(() {
                            isLanguageKorean = val;
                            _changeLanguage();
                          });
                          _studentLanguageController.text = language1;
                        },
                        controlAffinity: ListTileControlAffinity.leading,
                      ),
                      CheckboxListTile(
                        title: Text('English'),
                        value: isLanguageEnglish,
                        selected: isLanguageEnglish,
                        onChanged: (val) async {
                          String language2;
                          FocusScope.of(context).requestFocus(new FocusNode());
                          setState(() {
                            isLanguageEnglish = val;
                            _changeLanguage();
                          });
                          _studentLanguageController.text = language2;
                        },
                        controlAffinity: ListTileControlAffinity.leading,
                      ),
                      CheckboxListTile(
                        title: Text('Chinese'),
                        value: isLanguageChinese,
                        selected: isLanguageChinese,
                        onChanged: (val) async {
                          String language3;
                          FocusScope.of(context).requestFocus(new FocusNode());
                          setState(() {
                            isLanguageChinese = val;
                            _changeLanguage();
                          });
                          _studentLanguageController.text = language3;
                        },
                        controlAffinity: ListTileControlAffinity.leading,
                      ),
                      SizedBox(height: 5.0),
                      Text(
                        'Gender',
                        style: TextStyle(fontSize: 16.0),
                      ),
                      Row(
                        children: <Widget>[
                          _radioWithLabel(0, "Male"),
                          _radioWithLabel(1, "Female"),
                          _radioWithLabel(2, "Other"),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          RaisedButton(
                              color: Colors.lightGreen,
                              child: Text(
                                "ADD",
                                style: TextStyle(color: Colors.white),
                              ),
                              onPressed: () {
                                if (_formStateKey.currentState.validate()) {
                                  _formStateKey.currentState.save();
                                  dbHelperMP.add(Student2(
                                      studentsIdForUpdate,
                                      _studentName,
                                      _studentDepartment,
                                      _studentDoB,
                                      _studentLanguage,
                                      _studentGender));
                                }
                                _studentNameController.text = '';
                                _studentDepartmentController.text = '';
                                _studentDoBController.text = '';
                                _studentLanguageController.text = '';
                                _studentGenderController.text = '';
                                refreshStudentList();
                              }),
                          Padding(padding: EdgeInsets.all(10.0)),
                          RaisedButton(
                              color: Colors.red,
                              child: Text(
                                "CANCEL",
                                style: TextStyle(color: Colors.white),
                              ),
                              onPressed: () {
                                dbHelperMP.close();
                              }),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _radioWithLabel(int val, String label) {
    return Row(
      children: <Widget>[
        Radio(
          value: val,
          groupValue: _radioGroupValue,
          onChanged: (index) async {
            setState(() {
              _radioGroupValue = index;
              _student2.gender = _gender[index];
            });
            _studentGenderController.text = _student2.gender;
          },
        ),
        Text(label),
      ],
    );
  }
}
