import 'mp_student.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:io' as io;
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';

class DBHelperMP {
  static Database _database;

  Future<Database> get database async {
    if (_database != null) {
      return _database;
    }
    _database = await initDatabase();
    return _database;
  }

  initDatabase() async {
    io.Directory documentDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentDirectory.path, 'mpStudent2.db');
    var database = await openDatabase(path, version: 1, onCreate: _onCreate);
    return database;
  }

  _onCreate(Database db, int version) async {
    await db
        .execute('CREATE TABLE mpStudent2 (id INTEGER PRIMARY KEY, name Text,'
            ' department Text, DoB Text, language Text, gender Text)');
  }

  Future<Student2> add(Student2 student2) async {
    var dbClient = await database;
    student2.id = await dbClient.insert('mpStudent2', student2.toMap());
    return student2;
  }

  Future<List<Student2>> getStudent() async {
    var dbClient = await database;
    List<Map> maps = await dbClient.query('mpStudent2',
        columns: ['id', 'name', 'department', 'DoB', 'language', 'gender']);
    List<Student2> students2 = [];
    if (maps.length > 0) {
      for (int i = 0; i < maps.length; i++) {
        students2.add(Student2.forMap(maps[i]));
      }
    }
    return students2;
  }

  // ignore: missing_return
  Future<Student2> getInfo(int id) async {
    var dbClient = await database;
    List<Map> map = await dbClient.query('mpStudent2',
        columns: ['id', 'name', 'department', 'DoB', 'language', 'gender'],
        where: 'id = ?',
        whereArgs: [id]);
    Student2 student2 = Student2.forMap(map[0]);
    return student2;
  }

  Future<int> delete(int id) async {
    var dbClient = await database;
    return await dbClient.delete(
      'mpStudent2',
      where: 'id = ?',
      whereArgs: [id],
    );
  }

  Future<int> update(Student2 student2) async {
    var dbClient = await database;
    return await dbClient.update(
      'mpStudent2',
      student2.toMap(),
      where: 'id = ?',
      whereArgs: [student2.id],
    );
  }

  Future close() async {
    var dbClient = await database;
    dbClient.close();
  }
}
