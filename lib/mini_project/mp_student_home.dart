import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_5358992/mini_project/mp_student_add.dart';
import 'package:flutter_5358992/mini_project/mp_student_list.dart';

class MPStudentHome extends StatefulWidget {
  const MPStudentHome({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _MPStudentHomeState();
}

class _MPStudentHomeState extends State<MPStudentHome> {
  Widget _currentPage;
  int _currentIndex = 0;
  List _listPages = [];

  void _changePage(int selectedIndex) {
    setState(() {
      _currentIndex = selectedIndex;
      _currentPage = _listPages[selectedIndex];
    });
  }

  @override
  void initState() {
    super.initState();
    _listPages..add(MPStudentADD())..add(MPStudentListPage());
    _currentPage = MPStudentADD();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Student List"),
      ),
      body: SafeArea(
          child: Padding(
        padding: EdgeInsets.all(16.0),
        child: _currentPage,
      )),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentIndex,
        onTap: (selectedIndex) => _changePage(selectedIndex),
        items: [
          BottomNavigationBarItem(
              icon: Icon(Icons.perm_contact_cal_outlined),
              title: Text("Add Student")),
          BottomNavigationBarItem(
              icon: Icon(Icons.list_alt), title: Text("Student List")),
        ],
      ),
    );
  }
}
