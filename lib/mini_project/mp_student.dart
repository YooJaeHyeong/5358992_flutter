class Student2 {
  int id;
  String name;
  String department;
  String DoB;
  String language;
  String gender;

  Student2(this.id, this.name, this.department, this.DoB, this.language,
      this.gender);

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      'id': id,
      'name': name,
      'department': department,
      'DoB': DoB,
      'language': language,
      'gender': gender,
    };
    return map;
  }

  Student2.forMap(Map<String, dynamic> map) {
    id = map['id'];
    name = map['name'];
    department = map['department'];
    DoB = map['DoB'];
    language = map['language'];
    gender = map['gender'];
  }
}
