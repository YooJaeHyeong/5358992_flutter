import 'package:flutter/material.dart';
import 'package:flutter_5358992/mini_project/mp_student.dart';
import 'package:flutter_5358992/mini_project/mp_student_Detail.dart';
import 'package:flutter_5358992/mini_project/mp_student_Helper.dart';

class MPStudentListPage extends StatefulWidget {
  final int id;

  const MPStudentListPage({Key key, @required this.id}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _MPStudentListPageState();
}

class _MPStudentListPageState extends State<MPStudentListPage> {
  Future<List<Student2>> students2;
  DBHelperMP dbHelperMP;

  @override
  void initState() {
    super.initState();
    dbHelperMP = DBHelperMP();
    refreshStudentList();
  }

  refreshStudentList() {
    setState(() {
      students2 = dbHelperMP.getStudent();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          const Divider(height: 5.0),
          FutureBuilder(
              future: students2,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return generateList(snapshot.data);
                }
                if (snapshot.data == null || snapshot.data.length == 0) {
                  return Text('No Data Found');
                }
                return CircularProgressIndicator();
              })
        ],
      ),
    );
  }

  Widget generateList(List<Student2> student) {
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: SizedBox(
        width: double.infinity,
        child: DataTable(
          columns: [
            DataColumn(label: Text('NAME')),
            DataColumn(label: Text('VIEW')),
            DataColumn(label: Text('DELETE')),
          ],
          rows: student
              .map(
                (student) => DataRow(
                  cells: [
                    DataCell(Text(student.name)),
                    DataCell(
                      IconButton(
                        icon: Icon(Icons.chevron_right_sharp),
                        onPressed: () {
                          Navigator.of(context).push(
                            MaterialPageRoute(
                                builder: (context) => MPStudentDetail(
                                      id: widget.id,
                                    )),
                          );
                        },
                      ),
                    ),
                    DataCell(IconButton(
                      icon: Icon(Icons.delete),
                      onPressed: () {
                        dbHelperMP.delete(student.id);
                        refreshStudentList();
                      },
                    ))
                  ],
                ),
              )
              .toList(),
        ),
      ),
    );
  }
}
