/** Praticing set with color of flags*/
import 'dart:io';
void main(){
  // CREATE TWO SET THAT CONTAINS COLORS OF FLAGS OF ITALY AND BRITAIN
  // CREATE A SET THAT WILL CONTAIN
  // THE UNION, INTERSECTION, AND DIFFERENCE OF THE COLORS
  Set<String> flagOfBritain = {"Red","Blue","White"};
  Set<String> flagOfItaly = {"Red","Green","White"};
  Set<String> Venn_diagram = {"Union","Intersection","Difference"};
  // DISPLAY THE COLOR OR EACH FLAG
  print("The flag of Britain colors is $flagOfBritain");
  print("The flag of Italy colors is $flagOfItaly");
  // FIND AND DISPLAY INTERSECTION, AND DIFFERENCE
  // OF THE COLORS OF THE TWO FLAGS
  var InterSection = flagOfBritain.intersection(flagOfItaly);
  print("The intersection of color is $InterSection.");
  var Difference = {flagOfBritain.difference(flagOfItaly),
                    flagOfItaly.difference(flagOfBritain)};
  print("The difference of color is $Difference.");
  // FIND UNION AND ASSIGN IT THE UNION TO SET
  var Union = flagOfBritain.union(flagOfItaly);
  // DISPLAY THE UNION SET
  print("The Union colors is $Union");
  // PROMPT USER TO TYPE COLOR NAME
  String addColor, deleteColor;
  print("Type the color you want to add: ");
  addColor = stdin.readLineSync();
  Union.addAll([addColor]);
  // CHECK IF UNION SET CONTAINS COLOR AND DISPLAY THE RESULT
  print(Union);
  // PROMPT USER TYPE WHICH COLOR TO REMOVE
  print("Type the color you want to delete: ");
  deleteColor = stdin.readLineSync();
  Union.remove(deleteColor);
  // DELETE THAT COLOR AND DISPLAY THE RESULT
  print(Union);

  // DISPLAY Venn_diagram
  print("Union: $Union, Intersection: $InterSection, Difference: $Difference.");
}
