import 'dart:io';

main(){
  int hours;
  double rate, pay;


  stdout.write("How many hours did you work?: --> ");
  hours = int.parse(stdin.readLineSync());

  stdout.write("How much is the rate per hour?: --> ");
  rate = double.parse(stdin.readLineSync());

  pay = hours * rate;

  print("You have earned --> \$ ${pay}");
}