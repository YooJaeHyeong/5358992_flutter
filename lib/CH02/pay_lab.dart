import 'dart:io';

main(){
  double pay;
  int hours;
  final RATE = 5.0;

  stdout.write("How many hours did you work: --> ");
  hours = int.parse(stdin.readLineSync());

  pay = hours * RATE;

  print("You have earned --> \$ ${pay}");
  print("Is your hour even? --> \$ ${hours.toDouble()}");
}