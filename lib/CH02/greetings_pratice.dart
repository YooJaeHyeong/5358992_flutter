import 'dart:io';

void main(){
  String firstName = "";
  String lastName  = "";
  //This variable holds name of the user

  // To get user's name
  print("Enter Your First name: ");
  firstName = stdin.readLineSync();

  print("Enter Your Last name:");
  lastName = stdin.readLineSync();

  //Prints the greeting
  print("Hello $firstName $lastName");
}