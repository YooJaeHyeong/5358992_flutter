// The goal of this program is to
// practice operations in dart
void main(){
  int num1 = 30, num2 = 20;
  double num3;
  bool result;

  num2 += num1;
  print("Num2 is --> $num2");

  num3 = num1 / num2;
  print("num1/num2 is --> $num3");

  result = (num1 >= num2);

  print("Is num1 greater than or equal to num2 --> $result");

  num1 = num1<<2;
  print("num1 is now --> $num1");

  num3 = num2*num1/num2;
  print("num1/num2 is --> $num3");

  num3= num2 - num1 *num2 *num1 /num1;
  print("$num3");

}