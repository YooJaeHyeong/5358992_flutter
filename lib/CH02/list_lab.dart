/* Praticing list with color of months */
import 'dart:io';
void main() {
  String type1;
  int start, end;
  // CREATE A LIST THAT CONTAIN 5 MONTHS
  // JANUARY, FEBRUARY, APRIL, MAY, JUNE
  List<String> monthList = ["JANUARY", "FEBRUARY", "APRIL", "MAY", "JUN"];
  // ADD "JULY" TO THE LIST
  monthList.add("JULY");
  print("$monthList");
  // INSERT "MARCH" AFTER FEBRUARY THEN DISPLAY THE LIST
  monthList.insert(2, "MARCH");
  print("$monthList");
  // ADD "NOVEMBER" AND "DECEMBER" THEN DISPLAY THE LIST
  monthList.add("NOVEMBER, DECEMBER");
  print("$monthList");
  // INSERT "AUGUST", "SEPTEMBER", AND "OCTOBER" AFTER JULY THEN
  // DISPLAY THE LIST
  monthList.insertAll(7, ["AUGUST", "OCTOBER", "SEPTEMBER"]);
  print("$monthList");
  // PROMPT USER TO TYPE WHICH MONTH TO DELETE
  print("Type the month you want to delete: ");
  type1 = stdin.readLineSync();
  monthList.remove(type1);
  // DELETE THE MATCHING MONTH THEN DISPLAY THE LIST
  print("$monthList");
  // PROMPT USER TO TYPE START AND END OF RANGE TO DELETE
  print("Type the  month range you want to delete of start: ");
  start= int?.parse(stdin.readLineSync());
  print("Type the month range you want to delete of end: ");
  end = int?.parse(stdin.readLineSync());
  monthList.removeRange(start,end);

  // DELETE MONTHS IN RANGE THEN DISPLAY THE LIST
  print("$monthList");
}