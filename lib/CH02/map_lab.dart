/** Praticing map with phone book */
import "dart:io";

void main(){

  // CREATE TWO VARIABLES KEY AND VALUE
    String userName, phoneNumber;
  // CREATE AN EMPTY MAP TO HOLD USER NAME AND PHONE NUMBER
    Map<String, String> phoneBook = new Map();
  // CHECK IF THE MAP IS EMPTY AND DISPLAY THE RESULT
    print("$phoneBook");
  // A. PROMPT USER TO TYPE USER NAME AND ASSIGN IT TO KEY
    print("Type the name: ");
    userName = stdin.readLineSync();
  // B. PROMPT USER TO TYPE PHONE NUMBER AND ASSIGN IT TO VALUE
    print("Type the Phone Number: ");
    phoneNumber = stdin.readLineSync();
  // C. UPDATE THE MAP USING KEY AND VALUE THEN DISPLAY MAP
    phoneBook[userName] = phoneNumber;
    print(phoneBook);
  // DO A, B, AND C THREE TIMES
    print("Type the name: ");
    userName = stdin.readLineSync();
    print("Type the Phone Number: ");
    phoneNumber = stdin.readLineSync();
    phoneBook[userName] = phoneNumber;

    print("Type the name: ");
    userName = stdin.readLineSync();
    print("Type the Phone Number: ");
    phoneNumber = stdin.readLineSync();
    phoneBook[userName] = phoneNumber;

    print("Type the name: ");
    userName = stdin.readLineSync();
    print("Type the Phone Number: ");
    phoneNumber = stdin.readLineSync();
    phoneBook[userName] = phoneNumber;
    print(phoneBook);
  // PROMPT USER WHICH NAME TO DELETE
    String delete;
    print("Type the name to delete information: ");
    delete = stdin.readLineSync();
    phoneBook.remove(delete);
  // DELETE THAT USER AND DISPLAY THE THE MAP
    print(phoneBook);
}


