import 'dart:io';

void main(){
  String name = ""; //This variable holds name of the user

  // To get user's name
  print("Enter Your name: ");
  name = stdin.readLineSync();

  //Prints the greeting
  print("Hello $name");
}