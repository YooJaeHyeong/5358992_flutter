import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_5358992/CH10/navi/navi_page.dart';

void main() => runApp(NavigatorApp());

class NavigatorApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Navigator',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(primarySwatch: Colors.lightGreen),
      home: NavigatorHomePage(),
    );
  }
}
