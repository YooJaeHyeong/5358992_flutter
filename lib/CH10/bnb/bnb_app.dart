import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_5358992/CH10/bnb/bnb_home.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'BottomNavigationBar',
      home: Home(),
    );
  }
}
