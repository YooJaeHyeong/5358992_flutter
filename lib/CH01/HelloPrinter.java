package CH01;

import javax.swing.*;

// Display a greeting in the console window
public class HelloPrinter {
    public static void main(String[] args){
        System.out.println("Hello, World!");
    }
}
