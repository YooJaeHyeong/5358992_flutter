import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class WidgetTreePage extends StatefulWidget {
  WidgetTreePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _WidgetTreePageState createState() => _WidgetTreePageState();
}

class _WidgetTreePageState extends State<WidgetTreePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Container(
                      color: Colors.white,
                      height: 40.0,
                      width: 40.0,
                    ),
                    Padding(
                      padding: EdgeInsets.all(16.0),
                    ),
                    Expanded(
                        child: Container(
                      color: Colors.amber,
                      height: 40.0,
                      width: 40.0,
                    ))
                  ],
                ),
                Row(
                  children: <Widget>[
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        Container(
                          color: Colors.yellow,
                          height: 60.0,
                          width: 60.0,
                        ),
                        Padding(padding: EdgeInsets.all(16.0)),
                        Container(
                          color: Colors.amber,
                          height: 40.0,
                          width: 40.0,
                        ),
                        Padding(padding: EdgeInsets.all(16.0),),
                        Container(
                          color: Colors.brown,
                          height: 20.0,
                          width: 20.0,
                        )
                      ],
                    )
                  ],
                ),
                Row(
                  children: <Widget>[
                    CircleAvatar(
                      backgroundColor: Colors.lightGreen,
                      radius: 100.0,
                      child: Stack(
                        children: <Widget>[
                          Container(
                            height: 100.0,
                            width: 100.0,
                            color: Colors.yellow,
                          ),
                          Container(
                            height: 60.0,
                            width: 60.0,
                            color: Colors.amber,
                          ),
                          Container(
                            height: 40.0,
                            width: 40.0,
                            color: Colors.brown,
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
            padding: EdgeInsets.all(16.0),
          ),
        ),
      ),
    );
  }
}
