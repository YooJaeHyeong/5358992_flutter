import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'tree_home.dart';

void main() => runApp(WidgetTreeApp());

class WidgetTreeApp extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Widget tree Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      home: WidgetTreePage(title: 'Tree'),
    );
  }
}