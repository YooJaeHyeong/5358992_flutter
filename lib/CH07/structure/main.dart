import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_5358992/CH07/structure/home.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Domo HomePage'),
    );
  }
}