import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Card Center Padding'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            const CenterCardPadding(),
            const ExpandedFlexiblePositionedWidget()
          ],
        ),
      ),
    );
  }
}

class CenterCardPadding extends StatelessWidget {
  const CenterCardPadding({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        color: Colors.lightGreen,
        child: Padding(
          padding: EdgeInsets.all(100.0),
          child: Text(
            'Hello World',
            style: TextStyle(fontSize: 30),
          ),
        ),
      ),
    );
  }
}

class ExpandedFlexiblePositionedWidget extends StatelessWidget {
  const ExpandedFlexiblePositionedWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 500,
      child: Stack(
        children: [
          Column(
            children: [
              Row(
                children: [
                  Expanded(
                    child: TextButton(
                      onPressed: () {},
                      child: Text('Expanded'),
                      style: TextButton.styleFrom(
                        primary: Colors.white,
                        backgroundColor: Colors.green,
                        onSurface: Colors.grey,
                      ),
                    ),
                  ),
                  TextButton(
                    onPressed: () {},
                    child: Text('Not Expanded'),
                    style: TextButton.styleFrom(
                      primary: Colors.white,
                      backgroundColor: Colors.green,
                      onSurface: Colors.grey,
                    ),
                  )
                ],
              ),
              Row(
                children: [
                  Expanded(
                    child: TextButton(
                      onPressed: () {},
                      child: Text('Expanded'),
                      style: TextButton.styleFrom(
                        primary: Colors.white,
                        backgroundColor: Colors.teal,
                        onSurface: Colors.grey,
                      ),
                    ),
                  ),
                  Expanded(
                    child: TextButton(
                      onPressed: () {},
                      child: Text('Expanded'),
                      style: TextButton.styleFrom(
                        primary: Colors.white,
                        backgroundColor: Colors.teal,
                        onSurface: Colors.grey,
                      ),
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  Flexible(
                    fit: FlexFit.tight,
                    child: TextButton(
                      onPressed: () {},
                      child: Text('FlexFit.tight'),
                      style: TextButton.styleFrom(
                        primary: Colors.white,
                        backgroundColor: Colors.teal,
                        onSurface: Colors.grey,
                      ),
                    ),
                  ),
                  Flexible(
                    child: TextButton(
                      onPressed: () {},
                      child: Text('FlexFit.tight'),
                      style: TextButton.styleFrom(
                        primary: Colors.white,
                        backgroundColor: Colors.teal,
                        onSurface: Colors.grey,
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
          Positioned(
            left: 30,
            top: 0,
            child: Container(
              color: Colors.lightGreen,
              height: 150,
              width: 150,
              child: Row(
                children: [
                  Expanded(
                    child: TextButton(
                      onPressed: () {},
                      child: Text('Expanded'),
                      style: TextButton.styleFrom(
                        primary: Colors.white,
                        backgroundColor: Colors.teal,
                        onSurface: Colors.grey,
                      ),
                    ),
                  ),
                  TextButton(
                    onPressed: () {},
                    child: Text('Not Expanded'),
                    style: TextButton.styleFrom(
                      primary: Colors.white,
                      backgroundColor: Colors.green,
                      onSurface: Colors.grey,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
