import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_5358992/CH09/orient/orient_home.dart';

void main() => runApp(OrientationApp());

class OrientationApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Orientation App',
      home: OrientationPage(),
    );
  }
}
