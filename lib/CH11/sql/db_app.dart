import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_5358992/CH11/sql/db_home.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'SQLite CURD',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.lightGreen,
      ),
      home: StudentPage(),
    );
  }
}
