class FontInfo {
  String inputString;
  String family;
  double size;

  FontInfo({this.inputString, this.family = "Serif", this.size = 50});
}
