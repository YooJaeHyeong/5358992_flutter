/** This Program shows scope of a variable */
String global = "I am global";

void main(){

  void localFunction(){

    String local = "I am local";

    // print global from localFunction
    print("From localFunction(): $global");

    // print local from localFunction
    print("From localFunction(): $local");

  }
  // print global from main
  print("From main(): $global");

  // call function
  localFunction();
  // print local from main -> Not possible
  // uncomment to see the error
  //print("From main(): $local");
  //the reason for the error: Error: Getter not found: 'local'.
  //Undefined name 'local' because Not defined.
}