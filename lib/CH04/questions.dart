class Questions
{  //variables to store question and answer texts
  String questionText, correctResponse;

  Questions()
  { //Constructs a question with empty strings.
    questionText = "";
    correctResponse = "";
  }
  // Sets the question text. @param questionText the text of this question
  setQuestionText(String qText) => questionText = qText;
  // Sets the answer for this question. @param correctResponse the answer
  setQuestionAns(String aText) => correctResponse = aText;

  bool checkAnswer(String response)
  {
    return response == correctResponse;
  }
  display() => print(questionText);
}

