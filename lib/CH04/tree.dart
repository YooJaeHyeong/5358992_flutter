import 'dart:io';

class Tree
{
  static int treeCount = 0; //Counts how many trees
  String treeName;

  Tree(String name)
  {
    this.treeName = name;
    treeCount += 1; // Increments tree every time an object is created
  }
  //Get the names of trees
  String getTreeName(){return treeName;}
  //Returns the number of trees (Private in library)
  static int _getTreeCount(){return treeCount;}
}

void main()
{
  Tree t1 = new Tree("pine");
  Tree t2 = new Tree("oak");
  Tree t3 = new Tree("elm");

  print("t1 is: ${t1.getTreeName()}");
  print("t2 is: ${t2.getTreeName()}");
  print("t3 is: ${t3.getTreeName()}");
  print("There are: ${Tree._getTreeCount()} trees");
}