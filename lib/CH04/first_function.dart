double f_Of_XY([int x=0, int y=1]){
  // compute the value of z
  double z = (x * x) + (x / y);
  // return the result
  return z;
}

void main(){
  double result1;

  // calling the function
  result1 = f_Of_XY();

  //print the result
  print("f of 0, 1 is: $result1");
  print("f of 5, 8 is: ${f_Of_XY(5, 8)}");
}


