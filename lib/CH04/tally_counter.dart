import 'dart:io';

class TallyCounter
{
  //Stores the count of how many
  var totalCount = 0;
 //Advances the value of totalCount by 1
  click()
  {
   totalCount += 1;
  }
  //Returns the value of totalCount
  getTotal()
  {
    return totalCount;
  }
  reset()
  {
    totalCount = 0;
  }
}

void main()
{
  var concertCounter = new TallyCounter();
  TallyCounter queueCounter;

  concertCounter.click();
  concertCounter.click();
  concertCounter.click();
  print("People is concert are: ${concertCounter.getTotal()}");

  queueCounter = new TallyCounter();

  queueCounter.click();
  queueCounter.click();
  queueCounter.click();
  queueCounter.click();
  queueCounter.click();
  print("People Waiting queue are: ${queueCounter.getTotal()}");

  var num = concertCounter.getTotal() + queueCounter.getTotal();
  print("Total number expected people: $num");
}