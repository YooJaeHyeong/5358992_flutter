import 'dart:io';

int factoria(int number){
  if(number > 1)
    return number * factoria(number-1);

  return 1;
}

void main(){
  int num;
  stdout.write("Enter the number: ");
  num = int.parse(stdin.readLineSync());
  print("Factorial of $num is ${factoria(num)}");
}