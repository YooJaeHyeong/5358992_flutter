
String dot(String first, String second) => '$first.$second';
String snake_case(String first, String second) => '${first}_$second';

typedef String Join(String fName, String lName);

void main(){
  Join joinName;
  String firstName = "Jhon", lastName = "Smith";
  joinName = dot;
  print(joinName(firstName, lastName));
  joinName = snake_case;
  print(joinName(firstName, lastName));
}