import 'dart:io';

class CashRegister
{
  var totalPrice , itemCount;
  CashRegister()
  {
    totalPrice = 0.0;
    itemCount = 0;
  }
  getCount()
  {
    return itemCount;
  }
  getTotal()
  {
    return totalPrice;
  }
  addItem(double price)
  {
    itemCount += 1;
    totalPrice += price;
  }
  clear()
  {
    itemCount = 0;
  }
}

void main()
{
  CashRegister register1 = new CashRegister();

  register1.addItem(1.95);
  register1.addItem(0.95);
  register1.addItem(2.50);

  print(register1.getCount());
  print("Expected is: 3");
  print(register1.getTotal());
  print("Expected is: 5.40");

  register1.clear();
}