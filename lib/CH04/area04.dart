/** Under CH04  create area04 complete the code using the
 * give template This program edit areaO3 using FUNCTIONS*/
import 'dart:io';
import 'dart:math';
// DEFINE THE CONSTANT NAME OF AUTHOR OF THE PROGRAM
// DEFINE THE CONSTANT STUDENT ID OF AUTHOR OF THE PROGRAM
// DEFINE THE CONSTANT PI HERE AND SET ITS VALUE TO 3.14159
String name = "YooJaeHyeong", studentId = "5358992", dm; //dm = display menu
double pi=3.14159,area, length, width, radius, baseStraight, height, result;
void main() {
  /**Write a function to display author Info.
   * printAuthor() // display authors name
   * displayMenu() // display menu */
  // DECLARE ALL NEEDED VARIABLES HERE. GIVE EACH ONE A DESCRIPTIVE
  // NAME AND AN APPROPRIATE DATA TYPE.
  void printAuthor() {
    print("Name: $name");
    print("Student ID: $studentId");
  }
  void displayMenu() {
    print("*** Choose the number   ***"
        "\n*** 1 -- square         ***"
        "\n*** 2 -- rectangle      ***"
        "\n*** 3 -- circle         ***"
        "\n*** 4 -- right triangle ***"
        "\n*** 5 -- quit           ***");
  }

  // WRITE STATEMENTS HERE TO DISPLAY THE 4 MENU CHOICES.
  printAuthor();
  displayMenu();

  // WRITE A STATEMENT HERE TO INPUT THE USER'S MENU CHOICE.
  stdout.write("Choose a number: ");
  dm = stdin.readLineSync();
  // USE AN SWITCH IF STATEMENT TO OBTAIN ANY NEEDED INPUT INFORMATION
  // AND COMPUTE AND DISPLAY THE AREA FOR EACH VALID MENU CHOICE.
  /** To calculate area of circle and right triangle use functions.
   *  circleArea(double)
   *  rightTriangleArea(double , double ) */
  circleArea(double result)
  {
    stdout.write("Type the radius of the circle: ");
    radius = double.parse(stdin.readLineSync());
    result = pi * radius;
    stdout.write("The circle area is $result");
  }

  rightTriangleArea(double baseStraight, double height)
  {
    stdout.write("Type a base straight line of Right Triangle:");
    baseStraight = double.parse(stdin.readLineSync());
    stdout.write("Type a Height of Right Triangle:");
    height = double.parse(stdin.readLineSync());
    area = 0.5*baseStraight*height;
    stdout.write("The Right Triangle area is $area");
  }
  var number = int.parse(dm);
  while (number < 1 || number > 5) {
    stdout.write("Invalid number, enter again: "); // line 25, choose a number
    number = int.parse(stdin.readLineSync());
  }
    switch (number) {
      case 1 :
        stdout.write("Type the length of the sides of the square: ");
        length = double.parse(stdin.readLineSync());
        area = length * length;
        stdout.write("The square area is $area");
        break;

      case 2 :
        stdout.write("Type the width: ");
        width = double.parse(stdin.readLineSync());
        stdout.write("Type the length: ");
        length = double.parse(stdin.readLineSync());
        area = width * length;
        stdout.write("The rectangle area is $area");
        break;

      case 3:
        circleArea(result);
        break;

      case 4:
        rightTriangleArea(baseStraight, height);
        break;

      case 5:
        stdout.write("Exit the Program.");
        break;
    }
    // IF AN INVALID MENU CHOICE WAS ENTERED, AN ERROR MESSAGE SHOULD
    // BE DISPLAYED BY DEFAULT

  }
