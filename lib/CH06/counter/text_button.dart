import 'package:flutter/material.dart';

// ignore: must_be_immutable
class TextButton1 extends StatelessWidget {
  final GestureTapCallback onPressed;
  String buttonText;

  TextButton1({@required this.onPressed, @required this.buttonText});

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      fillColor: Colors.blue,
      splashColor: Colors.lightBlueAccent,
      child: Text(
        buttonText,
        style: TextStyle(color: Colors.white, fontSize: 18),
      ),
      onPressed: onPressed,
    );
  }
}
